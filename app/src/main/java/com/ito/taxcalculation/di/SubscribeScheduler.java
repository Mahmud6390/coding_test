package com.ito.taxcalculation.di;

import javax.inject.Qualifier;

@Qualifier
public @interface SubscribeScheduler {
}
